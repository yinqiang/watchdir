package main

import (
	"fmt"
	"os"
	"os/signal"
)

func main() {
	watcher, err := NewWatcher()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error:%s\n", err.Error())
		return
	}
	defer watcher.Close()

	go func() {
		for {
			select {
			case ev, ok := <-watcher.Events:
				if !ok {
					return
				}
				fmt.Fprintf(os.Stdout, "Event:%s\n", ev.Path)
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				fmt.Fprintf(os.Stderr, "Error:%s\n", err.Error())
			}
		}
	}()

	watcher.WatchDir("./test")

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
}
